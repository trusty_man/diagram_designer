﻿var current_selected_provider_id = "Provider1";
var current_selected_table_id = "";

var current_selected_row = "";
var current_sel_row_num;

var provider_list = ["Provider1"];

var table_id_list = {"Provider1":[]};

var table_column_list = { "Provider1": {} };

var schema_sourcefilename_list = { "Provider1": {} };

var previous_provider = "";

var res = [];

jsPlumb.ready(function () {
    jsPlumb.importDefaults({
        ConnectionsDetachable: false
    });
});

$(document).ready(function () {
    $("#btn_select_diagram").click(function () {
        current_selected_provider_id = $("#provider_select").val();
        Display_table_name_list();
        redraw_all_table();
        redraw_all_connection();
    });

    $('#provider_select').change(function () {
        var str = $(this).val();
        $("#diagram_select").val(str);
    });

    $("input").focus(function () {
        $(this).removeClass("alert-input");
    });


    $("#btn-insTbl").click(function () {
        $("#table_pop").removeClass("no-display");
        $("#table_pop").addClass("overlay");
        $("#table_pop").addClass("block");

        $("#table_pop").find("input").removeClass("alert-input");
        $("#pop_tablename_input").val('');
        $("#input-schema").val('dbo');
        $("#input-sourcefilename").val('');

        location.href = "#table_pop";
    });

    $("#edit_tbl_Save_Ok").click(function () {

        var table_id = $("#edit_pop_tablename_input").val();

        var flag = 0;
        if ($("#edit_pop_tablename_input").val() == "") {
            $("#edit_pop_tablename_input").addClass("alert-input");
            flag = 1;
            return;
        }
        $("#edit_table_pop").find("input").each(function () {
            console.log("came");
            if ($(this).val() == "") {
                $(this).addClass("alert-input");
                flag = 1;
            }
        });

        if (flag == 1) {
            return;
        }

        var schema = $("#edit_input-schema").val();
        var sourcefilename = $("#edit_input-sourcefilename").val();

        //insert new id to table_id_list
        var map = new Object();
        for (var i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
            if (table_id_list[current_selected_provider_id][i] == current_selected_table_id) {
                map[table_id_list[current_selected_provider_id][i]] = table_id;
            }
            else{
                map[table_id_list[current_selected_provider_id][i]] = table_id_list[current_selected_provider_id][i];
            }
        }

        var obj = new Object();
        var obj1 = new Object();
        $.each(table_column_list[current_selected_provider_id], function (key, value) {
            obj[map[key]] = value;
        });
        $.each(schema_sourcefilename_list[current_selected_provider_id], function (key, value) {
            obj1[map[key]] = value;
        });
        table_column_list[current_selected_provider_id] = obj;
        schema_sourcefilename_list[current_selected_provider_id] = obj1;

        for (var i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
            if (table_id_list[current_selected_provider_id][i] == current_selected_table_id) {
                table_id_list[current_selected_provider_id][i] = table_id;
            }
        }

        current_selected_table_id = table_id;

        $("div#" + table_id).draggable(
        {
            drag: function () {
                jsPlumb.repaintEverything();
            }
        });

        Display_table_name_list();
        redraw_all_table();
        redraw_all_connection();

        $("#edit_table_pop").addClass("no-display");
        $("#edit_table_pop").removeClass("overlay");
        $("#edit_table_pop").removeClass("block");

        location.href = "#";
    });
    
    $(".edit_tbl_Save_Cancel").click(function () {
        $("#edit_table_pop").addClass("no-display");
        $("#edit_table_pop").removeClass("overlay");
        $("#edit_table_pop").removeClass("block");

        location.href = "#";
    });


    $("#tbl_Save_Ok").click(function () {
        var table_id = $("#pop_tablename_input").val();

        var flag = 0;
        if ($("#pop_tablename_input").val() == "") {
            $("#pop_tablename_input").addClass("alert-input");
            flag = 1;
            return;
        }
        table_id_list[current_selected_provider_id].forEach(function (element) {
            if (element == table_id) {
                $("#pop_tablename_input").addClass("alert-input");
                flag = 1;
                return;
            }
        });
        if ($("#pop_table_name_input").val() == '') {
            $("#pop_table_name_input").addClass("alert-input");
            flag = 1;
        }

        if ($("#input-schema").val() == '') {
            $("#input-schema").addClass("alert-input");
            flag = 1;
        }

        if (flag == 1) {
            return;
        }
        var str = '';
        str += '<div class="db_table" id="' + table_id + '">';
        str += '<table class="w3-table w3-border" id="table_' + table_id + '">';
        str += '<tr>';
        str += '<th>';
        str += table_id;
        str += '</th>';
        str += '<th>';
        str += '<i class="fas fa-pen" onclick="func_editTbl(' + "'" + table_id + "'" + ')"></i>';
        str += '<i class="fas fa-trash-alt" onclick="func_removeTbl(' + "'" + table_id + "'" + ')"></i>';
        str += '<i class="fas fa-plus-circle" onclick="func_addColPop(' + "'" + table_id + "'" + ')"></i>';
        str += '<i class="fas fa-key" onclick="func_editPriKey(' + "'" + table_id + "'" + ')"></i>';
        str += '</th>';
        str += '<th></th>';
        str += '</tr>';
        str += '</table>';
        str += '</div>';

        //insert new id to table_id_list
        table_id_list[current_selected_provider_id].push(table_id);
        table_column_list[current_selected_provider_id][table_id] = new Array();
        schema_sourcefilename_list[current_selected_provider_id][table_id] = new Object();
        schema_sourcefilename_list[current_selected_provider_id][table_id]["schema"] = $("#input-schema").val();
        schema_sourcefilename_list[current_selected_provider_id][table_id]["sourcefilename"] = $("#input-sourcefilename").val();

        $("#mainboard").children('.row').append(str);

        $("#mainboard").children(".row").children('div.db_table').each(function () {
            $(this).height($(this).children('table').height());
        });
        redraw_all_connection();

        $("div#" + table_id).draggable(
        {
            drag: function () {
                jsPlumb.repaintEverything();
            }
        });
        //$("table#" + table_id).draggable({
        //});
        //redraw_all_connection();
        Display_table_name_list();
        location.href = "#";
    });

    $(".tbl_Save_Cancel").click(function () {
        $("#table_pop").addClass("no-display");
        $("#table_pop").removeClass("overlay");
        $("#table_pop").removeClass("block");

        location.href = "#";
    });



    $(".popup_add_column").click(function () {
        add_row();
    });

    $("#addOnePop").click(function () {
        var flag = 0;

        if ($("#mainrow").children(".subrow:last-child").children("input").val() == "") {
            $("#mainrow").children(".subrow:last-child").children("input").addClass("alert-input");
            flag = 1;
        }

        else {
            $("#mainrow").children('.subrow').each(function () {
                if ($(this)[0] != $("#mainrow").children(".subrow:last-child")[0]) {
                    if ($(this).children("input").val() == $("#mainrow").children(".subrow:last-child").children("input").val()) {
                        $("#mainrow").children(".subrow:last-child").children("input").addClass("alert-input");
                        flag = 1;
                    }
                }
            });
        }

        if (flag == 1) {
            return;
        }

        var str = "";
        str += '<div class="subrow">';
        str += '<input type="text" placeholder="field_name" class="col-md-4" />';
        str += '<select type="text" class="col-md-4 col-md-offset-1">';
        str += '<option>CHAR</option>';
        str += '<option>VARCHAR</option>';
        str += '<option>NVARCHAR</option>';
        str += '<option>BIT</option>';
        str += '<option>TINYINT</option>';
        str += '<option>SMALLINT</option>';
        str += '<option>INT</option>';
        str += '<option>BIGINT</option>';
        str += '<option>DECIMAL</option>';
        str += '<option>NUMERIC</option>';
        str += '<option>FLOAT</option>';
        str += '<option>REAL</option>';
        str += '<option>SMALLMONEY</option>';
        str += '<option>MONEY</option>';
        str += '<option>DATE</option>';
        str += '<option>DATETIME</option>';
        str += '<option>DATETIME2</option>';
        str += '<option>SMALLDATETIME</option>';
        str += '<option>TIME</option>';
        str += '<option>DATETIMEOFFSET</option>';
        str += '<option>TEXT</option>';
        str += '<option>NTEXT</option>';
        str += '<option>BINARY</option>';
        str += '<option>VARBINARY</option>';
        str += '<option>IMAGE</option>';
        str += '</select>';
        str += '<span>PK</span><input type="checkbox" />'
        str += '<span class="fas fa-trash-alt colDelCls col-md-1"></span>';
        str += '</div>';
        $("#mainrow").append(str);

        $("input").focus(function () {
            $(this).removeClass("alert-input");
        });

        $(".colDelCls").off("click");
        $(".colDelCls").on("click");
        $(".colDelCls").click(function () {
            var len = $(".colDelCls").length;
            if (len != 1) {
                $(this).parent().remove();
            }
            else {
                alert("You cannot delete this column!!!");
            }
        });
    });

    $("#Add_Col_Save_Ok").click(function () {
        var flag = 0;

        if ($("#mainrow").children(".subrow:last-child").children("input").val() == "") {
            $("#mainrow").children(".subrow:last-child").children("input").addClass("alert-input");
            flag = 1;
        }

        else {
            $("#mainrow").children('.subrow').each(function () {
                var cur_input = $(this).children("input");

                for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
                    if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"] == $(this).val()) {
                        cur_input.addClass("alert-input");
                        flag = 1;
                    }
                }
            });
        }

        if (flag == 1) {
            return;
        }

        var str = '';

        var curNum = table_column_list[current_selected_provider_id][current_selected_table_id].length;
        var newArr = [];
        for (i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
            if (table_column_list[current_selected_provider_id][current_selected_table_id][i]['Column Name'] == $("#select_after").val()) {
                curNum = i;
            }
        }

        console.log(curNum);

        for (j = curNum; j < table_column_list[current_selected_provider_id][current_selected_table_id].length; j++) {
            var ssss = table_column_list[current_selected_provider_id][current_selected_table_id][j];
            var arr = new Object({ "Column Name": ssss["Column Name"], "DataType": ssss["DataType"], "Length": ssss["Length"], "Scale": ssss["Scale"], "Nullable": ssss["Nullable"], "Default Value": ssss["Default Value"], "IsIdentity": ssss["IsIdentity"], "IsVersioning": ssss["IsVersioning"], "IsDateStamp": ssss["IsDateStamp"], "IsCalculated": ssss["IsCalculated"], "ValidateFor": ssss["ValidateFor"], "EnableDate": ssss["EnableDate"], "DisableDate": ssss["DisableDate"], "RenameDate": ssss["RenameDate"], "RenameColumnAs": ssss["RenameColumnAs"], "RenameDWColumnAs": ssss["RenameDWColumnAs"], "IsIndexed": ssss["IsIndexed"], "Remarks": ssss["Remarks"], "IsPrimary": ssss["IsPrimary"], "SurrogateFKName": ssss["SurrogateFKName"], "PKColumnRank": ssss["PKColumnRank"], "SourceColumn": ssss["SourceColumn"] });
            newArr.push(arr);
            table_column_list[current_selected_provider_id][current_selected_table_id].splice(j, 1);
        }

        $("#mainrow").children('.subrow').each(function () {
            var IsPrimary = ($(this).children('input:checked').val() == "on") ? 'Yes' : 'No';

            var arr = new Object({ "Column Name": $(this).children('input:nth-child(1)').val(), "DataType": $(this).children('select').val(), "Length": "", "Scale": "", "Nullable": "No", "Default Value": "", "IsIdentity": "No", "IsVersioning": "No", "IsDateStamp": "No", "IsCalculated": "No", "ValidateFor": "", "EnableDate": "", "DisableDate": "", "RenameDate": "", "RenameColumnAs": "", "RenameDWColumnAs": "", "IsIndexed": "No", "Remarks": "", "IsPrimary": IsPrimary, "SurrogateFKName": "", "PKColumnRank": "", "SourceColumn": ""});

            table_column_list[current_selected_provider_id][current_selected_table_id].push(arr);

            if (IsPrimary == 'Yes') {
                str += '<tr class="row-primarykey custom-row">';
                str += '<td>';
                str += $(this).children('input').val();
                str += '</td>';
                str += '<td>';
                str += $(this).children('select').val();
                str += '</td>';
                str += '<td>';
                str += '<i class="fas fa-minus-circle delete_rel_row"></i>';
                str += '</td>';
                str += '</tr>';
            }
            else {
                str += '<tr class="body-row custom-row">';
                str += '<td>';
                str += $(this).children('input').val();
                str += '</td>';
                str += '<td>';
                str += $(this).children('select').val();
                str += '</td>';
                str += '<td>';
                str += '<i class="fas fa-key manage_rel_row"></i>';
                str += '<i class="fas fa-minus-circle delete_rel_row"></i>';
                str += '</td>';
                str += '</tr>';
            }
        });

        for (var k = 0; k < newArr.length; k++) {
            table_column_list[current_selected_provider_id][current_selected_table_id].push(newArr[k]);
        }

        //$("#" + current_selected_table_id).children('table').children("tbody").append(str);
        redraw_table();
        redraw_all_connection();
        
        $(".custom-row").click(function (evt) {
            if (evt["target"]["className"].split(" ")[2] == "manage_rel_row") {
                current_selected_table_id = $(this).parent().parent().parent().attr("id");
                var current_column = $(this).children("td:nth-child(1)").html();
                $("#rel_table_name").html(current_selected_table_id);
                $("#rel_current_column").html(current_column);

                func_ManageRel(current_column);
            }
            
            else if (evt["target"]["className"].split(" ")[2] == "delete_rel_row") {
                
                current_selected_table_id = $(this).parent().parent().parent().attr("id");

                $(this).remove();
                var current_column = $(this).children("td:nth-child(1)").html();
                for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
                    if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"] == current_column) {
                        table_column_list[current_selected_provider_id][current_selected_table_id].splice(i, 1);
                    }
                }

                redraw_all_connection();
            }

            else {
                current_selected_row = $(this);
                $("tr").each(function () {
                    $(this).removeClass("selected-row");
                });
                $(this).addClass("selected-row");

                display_row_property(this);
            }
            
        });

        $("#column_pop").removeClass("block");
        $("#column_pop").removeClass("overlay");
        $("#column_pop").addClass("no-display");

        location.href = "#";
    });

    $(".Add_Col_Save_Cancel").click(function () {
        $("#column_pop").removeClass("block");
        $("#column_pop").removeClass("overlay");
        $("#column_pop").addClass("no-display");
        
        location.href = "#";
    });


    $(".btn_relation_cancel").click(function(){
        $("#ManRel_pop").addClass('no-display');
        $("#ManRel_pop").removeClass('overlay');
        $("#ManRel_pop").removeClass('block');

        location.href = "#";
    });

    $("#btn_rel_new").click(function () {
        table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["IsPrimary"] = $("#rel_target_select").val();

        var temp = $("#rel_target_select").val().split('.');

        //var tar = "table-" + current_selected_table_id;
        //var sor = "table-" + temp[0];

        jsPlumb.connect({
            source: current_selected_table_id,
            target: temp[0],
            connector: ["Flowchart", { stub: "10", gap: "10" }],
            anchors: ["Continuous"],
            endpoint: "Rectangle",
            endpointStyle: { fillStyle: "balck" }
        });

        $("#ManRel_pop").removeClass("block");
        $("#ManRel_pop").addClass("overlay");
        $("#ManRel_pop").addClass("no-display");
        location.href = "#";
    });

    $("#btn_rel_remove").click(function () {
        table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["IsPrimary"] = "No";
        redraw_all_connection();

        $("#ManRel_pop").removeClass("block");
        $("#ManRel_pop").removeClass("overlay");
        $("#ManRel_pop").addClass("no-display");
        location.href = "#";
    });

    $("#btn_priSettingsOk").click(function(){
        var obj = $("#pri_settings_tbl").children("tbody").children('tr');
        for (var i = 0; i < obj.length; i++) {
            var flag = 0;
            if ($("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(3)').find('input:checked').val() == "on" && $("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(4)').find('input').val() == "") {
                $("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(4)').find('input').addClass('alert-input');
                flag = 1;
                return;
            }
            if (flag == 1) {
                return;
            }
            var IsPrimary = ($("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(3)').find('input:checked').val() == "on") ? 'Yes' : 'No';

            var PKColumnRank = $("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(4)').find('input').val();

            if ($("#pri_settings_tbl").children("tbody").children('tr:nth-child(' + (i + 1) + ')').children('td:nth-child(3)').find('input:checked').val() != "on") {
                PKColumnRank = "";
            }

            table_column_list[current_selected_provider_id][current_selected_table_id][i]["PKColumnRank"] = PKColumnRank;

            if (IsPrimary == 'Yes') {
                table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] = IsPrimary;
            }

            else {
                if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] == 'Yes') {
                    table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] = IsPrimary;
                }
            }
        }

        redraw_table();
        $("#pop_primaryKeySettings").removeClass("block");
        $("#pop_primaryKeySettings").removeClass("overlay");
        $("#pop_primaryKeySettings").addClass("no-display");

        location.href = "#";
       
    });

    $(".btn_priSettingCancel").click(function () {
        $("#pop_primaryKeySettings").removeClass("block");
        $("#pop_primaryKeySettings").removeClass("overlay");
        $("#pop_primaryKeySettings").addClass("no-display");

        location.href = "#";
    });


    $("#btn-change-val").click(function () {
        var t = table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num];

        var t1 = ($("div#Nullable").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t2 = ($("div#IsIdentity").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t3 = ($("div#IsVersioning").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t4 = ($("div#IsDateStamp").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t5 = ($("div#IsCalculated").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t6 = ($("div#Indexed").children('input:checked').val() == "on") ? 'Yes' : 'No';
        var t7 = t['IsPrimary'];

        var obj = new Object({ "Column Name": $("div#ColumnName").children('input').val(), "DataType": $("div#DataType").children('select').val(), "Length": $("div#Length").children('input').val(), "Scale": $("div#Scale").children('input').val(), "Nullable": t1, "Default Value": $("div#DefaultValue").children('input').val(), "IsIdentity": t2, "IsVersioning": t3, "IsDateStamp": t4, "IsCalculated": t5, "ValidateFor": $("div#ValidateFor").children('input').val(), "EnableDate": $("div#EnableDate").children('input').val(), "DisableDate": $("div#DisableDate").children('input').val(), "RenameDate": $("div#RenameDate").children('input').val(), "RenameColumnAs": $("div#RenameColumnAs").children('input').val(), "RenameDWColumnAs": $("div#RenameDWColumnAs").children('input').val(), "IsIndexed": t6, "Remarks": $("div#Remarks").children('input').val(), "IsPrimary": t7, "SurrogateFKName": $("div#SurrogateFKName").children('input').val(), "PKColumnRank": table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["PKColumnRank"], "SourceColumn": $("div#SourceColumn").children('input').val() });

        table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num] = obj;

        redraw_table();
        redraw_all_connection();

        $("#row_pop_up").addClass("no-display");
        $("#row_pop_up").removeClass("block");
        $("#row_pop_up").removeClass("overlay");

        location.href = "#";
    });

    $(".btn-change-cancel").click(function () {
        $("#row_pop_up").addClass("no-display");
        $("#row_pop_up").removeClass("block");
        $("#row_pop_up").removeClass("overlay");
        
        location.href = "#";
    });

    $("#edit_provider_ok").click(function () {
        var flag = 0;

        if ($("#edit_provider_name").val() == "") {
            $("#edit_provider_name").addClass("alert-input");
            flag = 1;
        }

        for (var i = 0; i < provider_list.length; i++) {
            if ($("#edit_provider_name").val() == provider_list[i]) {
                $("#edit_provider_name").addClass("alert-input");
                flag = 1;
            }
        }

        if (flag == 1)
            return;

        var map = new Object();
        for (var i = 0; i < provider_list.length; i++) {
            if (previous_provider == provider_list[i]) {
                map[provider_list[i]] = $("#edit_provider_name").val();
            }
            else {
                map[provider_list[i]] = provider_list[i];
            }
        }
        
        var obj = new Object();
        $.each(table_column_list, function (key, value) {
            obj[map[key]] = value;
        });

        table_column_list = obj;

        obj = new Object();
        $.each(table_id_list, function (key, value) {
            obj[map[key]] = value;
        });

        table_id_list = obj;

        obj = new Object();
        $.each(schema_sourcefilename_list, function (key, value) {
            obj[map[key]] = value;
        });

        schema_sourcefilename_list = obj;

        for (i = 0; i < provider_list.length; i++) {
            if (previous_provider == provider_list[i]) {
                provider_list[i] = $("#edit_provider_name").val();
            }
        }

        $("#provider_select option").each(function () {
            if ($(this).html() == previous_provider) {
                $(this).html($("#edit_provider_name").val());
            }
        });

        $("#provider_select").val($("#edit_provider_name").val());

        if (current_selected_provider_id == previous_provider) {
            current_selected_provider_id = $("#edit_provider_name").val();

            Display_table_name_list();
            redraw_all_table();
            redraw_all_connection();
        }

        $("#edit_provider_pop").find('input').val('');
        $("#edit_provider_pop").find('input').removeClass('alert-input');
        $("#edit_provider_pop").removeClass("block");
        $("#edit_provider_pop").removeClass("overlay");
        $("#edit_provider_pop").addClass("no-display");
        location.href = "#";
    });

    $(".btn-edit-provider-cancel").click(function () {
        $("#edit_provider_pop").find('input').val('');
        $("#edit_provider_pop").find('input').removeClass('alert-input');
        $("#edit_provider_pop").removeClass("block");
        $("#edit_provider_pop").removeClass("overlay");
        $("#edit_provider_pop").addClass("no-display");
        location.href = "#";
    });

    $("#add_new_provider_ok").click(function () {
        var flag = 0;

        if ($("#new_provider_name").val() == "") {
            $("#new_provider_name").addClass("alert-input");
            flag = 1;
        }

        for (var i = 0; i < provider_list.length; i++) {
            if ($("#new_provider_name").val() == provider_list[i]) {
                $("#new_provider_name").addClass("alert-input");
                flag = 1;
            }
        }

        if (flag == 1)
            return;

        provider_list.push($("#new_provider_name").val());

        table_column_list[$("#new_provider_name").val()] = new Object();
        table_id_list[$("#new_provider_name").val()] = new Array();
        schema_sourcefilename_list[$("#new_provider_name").val()] = new Object();

        var str = '';
        str += '<option>' + $("#new_provider_name").val() + '</option>';
        $("#provider_select").append(str);

        $("#new_provider_pop").addClass("no-display");
        $("#new_provider_pop").removeClass("overlay");
        $("#new_provider_pop").removeClass("block");
        $("#new_provider_pop").find('input').val('');
        $("#new_provider_pop").find('input').removeClass('alert-input');

        location.href = "#";
    });

    $(".add_new_provider_cancel").click(function () {
        $("#new_provider_pop").addClass("no-display");
        $("#new_provider_pop").removeClass("overlay");
        $("#new_provider_pop").removeClass("block");
        $("#new_provider_pop").find('input').val('');
        $("#new_provider_pop").find('input').removeClass('alert-input');

        location.href = "#";
    });

    
});

function display_row_property(param) {
    current_selected_table_id = $(param).parent().parent().parent().attr('id');
    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        if ($(param).children("td:nth-child(1)").html() == table_column_list[current_selected_provider_id][current_selected_table_id][i]['Column Name']) {
            current_sel_row_num = i;
        }
    }
    var t = table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num];

    var str = '';
    str += '<div class="row" id="ColumnName">';
    str += '<span class="col-md-5">Column Name</span>';
    str += '<input class="col-md-7" type="text" value="' + t['Column Name'] + '" />';
    str += '</div>';
    str += '<div class="row" id="DataType">';
    str += '<span class="col-md-5">DataType</span>';
    str += '<select type="text" class="col-md-7">';
    str += '<option>CHAR</option>';
    str += '<option>VARCHAR</option>';
    str += '<option>NVARCHAR</option>';
    str += '<option>BIT</option>';
    str += '<option>TINYINT</option>';
    str += '<option>SMALLINT</option>';
    str += '<option>INT</option>';
    str += '<option>BIGINT</option>';
    str += '<option>DECIMAL</option>';
    str += '<option>NUMERIC</option>';
    str += '<option>FLOAT</option>';
    str += '<option>REAL</option>';
    str += '<option>SMALLMONEY</option>';
    str += '<option>MONEY</option>';
    str += '<option>DATE</option>';
    str += '<option>DATETIME</option>';
    str += '<option>DATETIME2</option>';
    str += '<option>SMALLDATETIME</option>';
    str += '<option>TIME</option>';
    str += '<option>DATETIMEOFFSET</option>';
    str += '<option>TEXT</option>';
    str += '<option>NTEXT</option>';
    str += '<option>BINARY</option>';
    str += '<option>VARBINARY</option>';
    str += '<option>IMAGE</option>';
    str += '</select>';
    str += '</div>';
    str += '<div class="row" id="Length">';
    str += '<span class="col-md-5">Length</span>';
    str += '<input type="number" class="col-md-7" value="' + t['Length'] + '"  />';
    str += '</div>';
    str += '<div class="row" id="Scale">';
    str += '<span class="col-md-5">Scale</span>';
    str += '<input type="text" class="col-md-7" value="' + t['Scale'] + '" />';
    str += '</div>';
    str += '<div class="row" id="Nullable">';
    str += '<span class="col-md-5">Nullable</span>';
    if (t['Nullable'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="DefaultValue">';
    str += '<span class="col-md-5">Default Value</span>';
    str += '<input class="col-md-7" type="text" value="' + t["Default Value"] + '" />';
    str += '</div>';
    str += '<div class="row" id="SourceColumn">';
    str += '<span class="col-md-5">SourceColumn</span>';
    str += '<input class="col-md-7" type="text" value="' + t["SourceColumn"] + '" />';
    str += '</div>';
    str += '<div class="row" id="IsIdentity">';
    str += '<span class="col-md-5">IsIdentity</span>';
    if (t['IsIdentity'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="IsVersioning">';
    str += '<span class="col-md-5">IsVersioning</span>';
    if (t['IsVersioning'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="IsDateStamp">';
    str += '<span class="col-md-5">IsDateStamp</span>';
    if (t['IsDateStamp'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="IsCalculated">';
    str += '<span class="col-md-5">IsCalculated</span>';
    if (t['IsCalculated'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="ValidateFor">';
    str += '<span class="col-md-5">ValidateFor</span>';
    str += '<input class="col-md-7" type="text" value="' + t['ValidateFor'] + '" />';
    str += '</div>';
    str += '<div class="row" id="EnableDate">';
    str += '<span class="col-md-5">EnableDate</span>';
    str += '<input class="col-md-7" type="datetime-local" min="2019-01-01T00:00" value="' + t['EnableDate'] + '" />';
    str += '</div>';
    str += '<div class="row" id="DisableDate">';
    str += '<span class="col-md-5">DisableDate</span>';
    str += '<input class="col-md-7" type="datetime-local" min="2019-01-01T00:00" value="' + t['DisableDate'] + '" />';
    str += '</div>';
    str += '<div class="row" id="RenameDate">';
    str += '<span class="col-md-5">RenameDate</span>';
    str += '<input class="col-md-7" type="datetime-local" min="2019-01-01T00:00" value="' + t['RenameDate'] + '" />';
    str += '</div>';
    str += '<div class="row" id="RenameColumnAs">';
    str += '<span class="col-md-5">RenameColumnAs</span>';
    str += '<input class="col-md-7" type="text" value="' + t['RenameColumnAs'] + '" />';
    str += '</div>';
    str += '<div class="row" id="RenameDWColumnAs">';
    str += '<span class="col-md-5">RenameDWColumnAs</span>';
    str += '<input class="col-md-7" type="text" value="' + t['RenameDWColumnAs'] + '" />';
    str += '</div>';
    str += '<div class="row" id="SurrogateFKName">';
    str += '<span class="col-md-5">SurrogateFKName</span>';
    str += '<input class="col-md-7" type="text" value="' + t['SurrogateFKName'] + '" />';
    str += '</div>';
    str += '<div class="row" id="Indexed">';
    str += '<span class="col-md-5">Indexed</span>';
    if (t['IsIndexed'] == "No") {
        str += '<input class="col-md-1" type="checkbox" />';
    }
    else {
        str += '<input class="col-md-1" type="checkbox" checked />';
    }
    str += '</div>';
    str += '<div class="row" id="Remarks">';
    str += '<span class="col-md-5">Remarks</span>';
    str += '<input class="col-md-7" type="text" value="' + t['Remarks'] + '" />';
    str += '</div>';

    str += '<hr />';
    //$("#right-section").html(str);
    $("#main_row").html(str);

    $("#DataType").children("select").val(t["DataType"].toUpperCase());
    console.log(t["DataType"]);

    $("#row_pop_up").removeClass("no-display");
    $("#row_pop_up").addClass("block");
    $("#row_pop_up").addClass("overlay");

    location.href = "#row_pop_up";
}


function func_addColPop(param) {
    var str = "";
    str += '<div class="subrow">';
    str += '<input type="text" placeholder="field_name" class="col-md-4" />';
    str += '<select type="text" class="col-md-4 col-md-offset-1">';
    str += '<option>CHAR</option>';
    str += '<option>VARCHAR</option>';
    str += '<option>NVARCHAR</option>';
    str += '<option>BIT</option>';
    str += '<option>TINYINT</option>';
    str += '<option>SMALLINT</option>';
    str += '<option>INT</option>';
    str += '<option>BIGINT</option>';
    str += '<option>DECIMAL</option>';
    str += '<option>NUMERIC</option>';
    str += '<option>FLOAT</option>';
    str += '<option>REAL</option>';
    str += '<option>SMALLMONEY</option>';
    str += '<option>MONEY</option>';
    str += '<option>DATE</option>';
    str += '<option>DATETIME</option>';
    str += '<option>DATETIME2</option>';
    str += '<option>SMALLDATETIME</option>';
    str += '<option>TIME</option>';
    str += '<option>DATETIMEOFFSET</option>';
    str += '<option>TEXT</option>';
    str += '<option>NTEXT</option>';
    str += '<option>BINARY</option>';
    str += '<option>VARBINARY</option>';
    str += '<option>IMAGE</option>';
    str += '</select>';
    str += '<span>PK</span><input type="checkbox" />'
    str += '<span class="fas fa-trash-alt colDelCls col-md-1"></span>';
    str += '</div>';
    $("#mainrow").html(str);

    current_selected_table_id = param;

    str = '';
    str += '<span class="col-md-5">Insert After</span>';
    str += '<select id="select_after" class="col-md-5">';
    for (i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        str += '<option>';
        str += table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"];
        str += '</option>';
    }
    str += '</select>';
    $("#firstrow").html(str);

    $("input").off("focus");
    $("input").on("focus");
    $("input").focus(function () {
        $(this).removeClass("alert-input");
    });
    

    $("#column_pop").removeClass("no-display");
    $("#column_pop").addClass("block");
    $("#column_pop").addClass("overlay");

    location.href = "#column_pop";
}

function func_deleteCol(param) {
    current_selected_table_id = param;
    //if (table_column_list[current_selected_diagram_id][current_selected_table_id].length == 1)
    //    return;

    if (current_selected_row != "") {
        current_selected_row.remove();

        for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
            if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"] == current_selected_row.children("td:nth-child(1)").html()) {
                if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] == 'Yes' || table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] == 'No')
                    table_column_list[current_selected_provider_id][current_selected_table_id].splice(i, 1);
                else
                    //alert('Something went wrong!\nYou must remove the relation of this column!!!');
                    console.log('Something went wrong!\nYou must remove the relation of this column!!!');
            }
        }
    }
}

function func_ManageRel(param) {
    $("#btn_rel_remove").removeAttr('disabled');

    var current_column = param;

    $("#rel_table_name").html("Table Name::" + current_selected_table_id);
    $("#rel_current_column").html("Current Column::" + current_column);

    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        if (current_column == table_column_list[current_selected_provider_id][current_selected_table_id][i]['Column Name']) {
            current_sel_row_num = i;
        }
    }
    var foregingkey = (table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["IsPrimary"] == 'Yes' || table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["IsPrimary"] == 'No') ? '' : table_column_list[current_selected_provider_id][current_selected_table_id][current_sel_row_num]["IsPrimary"];
    $("#rel_target").html(foregingkey);

    if (foregingkey == "") {
        $("#btn_rel_remove").attr('disabled', 'true');
    }

    var str1 = "";
    var str = '<div class="row"><span>TargetTable.Column</span></div>';
    str += '<div class="row"><select id="rel_target_select">';
    
    for (i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
        if (current_selected_table_id != table_id_list[current_selected_provider_id][i]) {
            for (var j = 0; j < table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]].length; j++) {
                if (table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]['IsPrimary'] == 'Yes') {
                    str1 += '<option>' + table_id_list[current_selected_provider_id][i] + '.' + table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["Column Name"] + '</option>';
                }
            }
        }
    }
    str += str1;
    str += '</select></div>';
    if (str1 == "") {
        str = "No Items To Display";
    }
    $("#rel_target_div").html(str);

    $("#ManRel_pop").removeClass('no-display');
    $("#ManRel_pop").addClass('block');
    $("#ManRel_pop").addClass('overlay');
    
    location.href = "#ManRel_pop";
    
}

function func_removeTbl(param) {
    current_selected_table_id = param;
    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] != 'Yes' && table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] != 'No') {
            return;
        }
    }

    $("#" + param).remove();
    table_id_list[current_selected_provider_id].pop(param);
    delete table_column_list[current_selected_provider_id][param];
    delete schema_sourcefilename_list[current_selected_provider_id][param];

    Display_table_name_list();
    redraw_all_connection();
}


//Primary Key Settings
function func_editPriKey(param) {
    current_selected_table_id = param;
    if (table_column_list[current_selected_provider_id][current_selected_table_id].length == 0) {
        alert("No Items to set as Primary Key!!!");
        return;
    }

    var str = "";
    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        
        str += '<tr class="body-row">';
        str += '<td>' + table_column_list[current_selected_provider_id][current_selected_table_id][i]['Column Name'] + '</td>';
        str += '<td>' + table_column_list[current_selected_provider_id][current_selected_table_id][i]['DataType'] + '</td>';
        if (table_column_list[current_selected_provider_id][current_selected_table_id][i]['IsPrimary'] == 'Yes') {
            str += '<td><input type="checkbox" checked /></td>';
        }
        else {
            str += '<td><input type="checkbox" /></td>';
        }
        str += '<td><input type="number" value="' + parseInt(table_column_list[current_selected_provider_id][current_selected_table_id][i]['PKColumnRank']) + '" /></td>';
        str += '</tr>';
    }
    $("#pri_settings_tbl").children("tbody").html(str);

    $("#pop_primaryKeySettings").removeClass("no-display");
    $("#pop_primaryKeySettings").addClass("block");
    $("#pop_primaryKeySettings").addClass("overlay");

    location.href = "#pop_primaryKeySettings";
}

function redraw_table() {
    var str = '';
    str += '<tr>';
    str += '<th>';
    str += current_selected_table_id;
    str += '</th>';

    str += '<th>';
    str += '<i class="fas fa-pen" onclick="func_editTbl(' + "'" + current_selected_table_id + "'" + ')"></i>';
    str += '<i class="fas fa-trash-alt" onclick="func_removeTbl(' + "'" + current_selected_table_id + "'" + ')"></i>';
    str += '<i class="fas fa-plus-circle" onclick="func_addColPop(' + "'" + current_selected_table_id + "'" + ')"></i>';
    str += '<i class="fas fa-key" onclick="func_editPriKey(' + "'" + current_selected_table_id + "'" + ')"></i>';
    str += '</th>';

    str += '<th></th>';
    str += '</tr>';

    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] == "Yes") {
            str += '<tr class="row-primarykey custom-row">';
            str += '<td>';
            str += table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"];
            str += '</td>';
            str += '<td>';
            str += table_column_list[current_selected_provider_id][current_selected_table_id][i]["DataType"];
            str += '</td>';
            str += '<i class="fas fa-minus-circle delete_rel_row"></i>';
            str += '<td></td>';
            str += '</tr>';
        }
        else {
            str += '<tr class="body-row custom-row">';
            str += '<td>';
            str += table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"];
            str += '</td>';
            str += '<td>';
            str += table_column_list[current_selected_provider_id][current_selected_table_id][i]["DataType"];
            str += '</td>';
            str += '<td>';
            str += '<i class="fas fa-key manage_rel_row"></i>';
            str += '<i class="fas fa-minus-circle delete_rel_row"></i>';
            str += '</td>';
            str += '</tr>';
        }
    }

    $("#" + current_selected_table_id).children('table').html(str);
    $("#mainboard").children(".row").children('div.db_table').each(function () {
        $(this).height($(this).children('table').height());
    });
    redraw_all_connection();
    $(".custom-row").click(function (evt) {
        if (evt["target"]["className"].split(" ")[2] == "manage_rel_row") {
            current_selected_table_id = $(this).parent().parent().parent().attr("id");
            var current_column = $(this).children("td:nth-child(1)").html();
            $("#rel_table_name").html(current_selected_table_id);
            $("#rel_current_column").html(current_column);

            func_ManageRel(current_column);
        }

        else if (evt["target"]["className"].split(" ")[2] == "delete_rel_row") {
            
            current_selected_table_id = $(this).parent().parent().parent().attr("id");

            $(this).remove();
            var current_column = $(this).children("td:nth-child(1)").html();
            for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
                if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"] == current_column) {
                    table_column_list[current_selected_provider_id][current_selected_table_id].splice(i, 1);
                }
            }

            redraw_all_connection();
        }

        else {
            current_selected_row = $(this);
            $("tr").each(function () {
                $(this).removeClass("selected-row");
            });
            $(this).addClass("selected-row");

            display_row_property(this);
        }
    });
}

$("#add_newRel").click(function () {
    var source = $("#select_source_id").val();
    var target = $("#select_target_id").val();

    for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
        if (source == table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"]) {
            var temp;
            temp = table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"].split("/");
            var temp1;
            temp1 = target.split('.');
            if (temp[0] == temp1[0] && temp[1] == temp1[1]) {
                //alert("This relation is already exist!!!");
                console.log("This relation is already exist!!!");
                return;
            }
            else if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] != 'Yes' && table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] !== 'No') {
                //alert("This column has already relation!\nPlease edit that relation!!!");
                console.log("This column has already relation!\nPlease edit that relation!!!");
                return;
            }
            else {
                table_column_list[current_selected_provider_id][current_selected_table_id][i]["IsPrimary"] = temp1[0] + '.' + temp1[1];
                location.href = "#";

                jsPlumb.connect({
                    source: current_selected_table_id,
                    target: temp1[0],
                    connector: ["Flowchart", { stub: "10", gap: "10" }],
                    anchors: ["Continuous"],
                    endpoint: "Rectangle",
                    endpointStyle: { fillStyle: "balck" }
                });
            }
        }
    }
});

function redraw_all_connection() {
    jsPlumb.deleteEveryConnection();
    jsPlumb.deleteEveryEndpoint();

    table_id_list[current_selected_provider_id].forEach(function (element) {

        var cur_table_id = element;

        for (var i = 0; i < table_column_list[current_selected_provider_id][element].length; i++) {

            if (table_column_list[current_selected_provider_id][element][i]["IsPrimary"] != "Yes" && table_column_list[current_selected_provider_id][element][i]["IsPrimary"] != "No") {
                
                var temp = table_column_list[current_selected_provider_id][element][i]["IsPrimary"].split(".");
                console.log("came:First Loop!!!" + temp[0] + "::" + temp[1]);
                var flag1 = 0;
                for (var k = 0; k < table_id_list[current_selected_provider_id].length; k++) {
                    console.log("came:Second Loop!!!");
                    if (table_id_list[current_selected_provider_id][k] == temp[0]) {
                        flag1 = 1;
                    }
                }

                if (flag1 == 1) {
                    var flag = 0;
                    for (var j = 0; j < table_column_list[current_selected_provider_id][temp[0]].length; j++) {
                        console.log("came:Third Loop!!!");
                        if (table_column_list[current_selected_provider_id][temp[0]][j]['Column Name'] == temp[1]) {
                            flag = 1;
                        }
                    }
                    if (flag == 1) {
                        jsPlumb.connect({
                            source: element,
                            target: temp[0],
                            connector: ["Flowchart", { stub: "5", gap: "5" }],
                            anchors: ["Continuous"],
                            endpoint: "Rectangle",
                            endpointStyle: { fillStyle: "balck" }
                        });
                    }
                }                
            }
        }

        for (i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
            $("div#" + table_id_list[current_selected_provider_id][i]).draggable(
            {
                drag: function () {
                    jsPlumb.repaintEverything();
                },
                containment: $(this).parent()
            });
        }
    });

}

function Display_table_name_list() {
    var str = '';
    str += '<span class="heading row">Table Names</span>';
    if (table_id_list[current_selected_provider_id].length == 0) {
        str += '<span class="row">No Tables!!!</span>';
    }
    else{
        for (var i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
            str += '<span class="row">' + table_id_list[current_selected_provider_id][i] + '</span>';
        }
    }
    
    $('#table-names').html(str);
}

function New_Provider() {
    $("#new_provider_pop").removeClass("no-display");
    $("#new_provider_pop").addClass("block");
    $("#new_provider_pop").addClass("overlay");

    location.href = "#new_provider_pop";
}

function redraw_all_table() { 
    var str = '';

    for (var i = 0; i < table_id_list[current_selected_provider_id].length; i++) {
        str += '<div';
        str += ' class="db_table" id="' + table_id_list[current_selected_provider_id][i] + '">';

        str += '<table class="w3-table w3-border" id="table_' + table_id_list[current_selected_provider_id][i] + '">';
        str += '<tr>';
        str += '<th>';
        str += table_id_list[current_selected_provider_id][i];
        str += '</th>';

        str += '<th>';
        str += '<i class="fas fa-pen" onclick="func_editTbl(' + "'" + table_id_list[current_selected_provider_id][i] + "'" + ')"></i>';
        str += '<i class="fas fa-trash-alt" onclick="func_removeTbl(' + "'" + table_id_list[current_selected_provider_id][i] + "'" + ')"></i>';
        str += '<i class="fas fa-plus-circle" onclick="func_addColPop(' + "'" + table_id_list[current_selected_provider_id][i] + "'" + ')"></i>';
        str += '<i class="fas fa-key" onclick="func_editPriKey(' + "'" + table_id_list[current_selected_provider_id][i] + "'" + ')"></i>';
        str += '</th>';
        str += '<th></th>';
        str += '</tr>';

        //console.log(table_id_list[current_selected_provider_id][i]);
        
        for (var j = 0; j < table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]].length; j++) {
            if (table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["IsPrimary"] == "Yes") {
                str += '<tr class="row-primarykey custom-row">';
                str += '<td>';
                str += table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["Column Name"];
                str += '</td>';
                str += '<td>';
                str += table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["DataType"];
                str += '</td>';
                str += '<td><i class="fas fa-minus-circle delete_rel_row"></i></td>';
                str += '</tr>';
            }
            else {
                str += '<tr class="body-row custom-row">';
                str += '<td>';
                str += table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["Column Name"];
                str += '</td>';
                str += '<td>';
                str += table_column_list[current_selected_provider_id][table_id_list[current_selected_provider_id][i]][j]["DataType"];
                str += '</td>';
                str += '<td>';
                str += '<i class="fas fa-key manage_rel_row"></i>';
                str += '<i class="fas fa-minus-circle delete_rel_row"></i>';
                str += '</td>';
                str += '</tr>';
            }
        }

        str += '</table>';
        str += '</div>';
    }

    $("#mainboard").children('.row').html('');
    $('#mainboard').children(".row").html(str);

    $("#mainboard").children(".row").children('div.db_table').each(function () {
        $(this).height($(this).children('table').height());
    });
    
    $(".custom-row").click(function (evt) {
        if (evt["target"]["className"].split(" ")[2] == "manage_rel_row") {
            current_selected_table_id = $(this).parent().parent().parent().attr("id");
            var current_column = $(this).children("td:nth-child(1)").html();
            $("#rel_table_name").html(current_selected_table_id);
            $("#rel_current_column").html(current_column);

            func_ManageRel(current_column);
        }

        else if (evt["target"]["className"].split(" ")[2] == "delete_rel_row") {
            
            current_selected_table_id = $(this).parent().parent().parent().attr("id");

            $(this).remove();
            var current_column = $(this).children("td:nth-child(1)").html();
            for (var i = 0; i < table_column_list[current_selected_provider_id][current_selected_table_id].length; i++) {
                if (table_column_list[current_selected_provider_id][current_selected_table_id][i]["Column Name"] == current_column) {
                    table_column_list[current_selected_provider_id][current_selected_table_id].splice(i, 1);
                }
            }

            redraw_all_connection();
        }

        else {
            current_selected_row = $(this);
            $("tr").each(function () {
                $(this).removeClass("selected-row");
            });
            $(this).addClass("selected-row");

            display_row_property(this);
        }

    });
}

function New_Diagram() {
    table_id_list[current_selected_provider_id] = new Array();
    table_column_list[current_selected_provider_id] = new Object();
    $("#mainboard").children(".row").html("");
    jsPlumb.deleteEveryConnection();
    jsPlumb.deleteEveryEndpoint();
    Display_table_name_list();
}

function Export_Diagram() {  
    var export_arr = [];
    for (var property in table_column_list) {
        for (var property1 in table_column_list[property]) {
            var tt = schema_sourcefilename_list[property][property1];
            var PkColumnRank = 1;
            for (var i = 0; i < table_column_list[property][property1].length; i++) {
                var IsPrimary = table_column_list[property][property1][i]["IsPrimary"] == 'Yes' ? 'Yes' : 'No';
                var foregingkey = (table_column_list[property][property1][i]["IsPrimary"] == 'Yes' || table_column_list[property][property1][i]["IsPrimary"] == 'No') ? '' : table_column_list[property][property1][i]["IsPrimary"];
                var t = table_column_list[property][property1][i];
                var t1 = table_column_list[property][property1][0];

                var pp = (table_column_list[property][property1][i]["IsPrimary"] == 'Yes') ? PkColumnRank : '';

                export_arr.push(new Object({ "Provider": property, "SourceFile": schema_sourcefilename_list[property][property1]["sourcefilename"], "TargetTable": schema_sourcefilename_list[property][property1]["schema"] + "." + property1, "SourceColumn": t["SourceColumn"], "TargetColumn": t["Column Name"], "DataType": t["DataType"], "Length": t["Length"], "Scale": t["Scale"], "IsNullable": t["Nullable"], "Default": t["Default Value"], "IsIdentity": t["IsIdentity"], "IsVersioning": t["IsVersioning"], "IsDateStamp": t["IsDateStamp"], "IsCalculated": t["IsCalculated"], "ValidateFor": t["ValidateFor"], "EnableDate": t["EnableDate"], "DisableDate": t["DisableDate"], "RenameDate": t["RenameDate"], "RenameColumnAs": t["RenameColumnAs"], "RenameDWColumnAs": t["RenameDWColumnAs"], "PKColumnRank": pp, "ClusteredIndexRank": "", "IsPrimary": IsPrimary, "ForeignKey": foregingkey, "SurrogateFKName": t["SurrogateFKName"], "IsIndexed": t["IsIndexed"], "Remarks": t["Remarks"], "PKColumnRank": t["PKColumnRank"] }));

                if (table_column_list[property][property1][i]["IsPrimary"] == 'Yes') {
                    PkColumnRank++;
                }
            }
        }
    }

    columnDelimiter = ',';
    lineDelimiter = '\n';

    keys = Object.keys(export_arr[0]);

    var result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    export_arr.forEach(function (item) {
        ctr = 0;
        keys.forEach(function (key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    var data, filename, link;

    var csv = result;
    if (csv == null) return;

    filename = 'export.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}

function Import_Diagram() {
    var str = '';
    str +='<div class="popup">';
    str +='<h2>Load Your Previous Works</h2>';
    str +='<button class="close btn-cancel">&times;</button>';
    str +='<hr />';

    str +='<div class="content">';
    str +='<div class="row">';
    str +='<div class="subrow">';
    str += '<input type="file" id="csv" multiple accept=".csv">';
    str +='</div>';
    str +='</div>';
    str +='<hr />';
    str +='</div>';

    str +='<div>';
    str +='<button id="btn_load_ok" disabled>Load</button>';
    str +='<button class="btn-cancel">Cancel</button>';
    str +='</div>';
    str +='</div>';
    $("#load_diagram_pop").html(str);

    $("#load_diagram_pop").removeClass("no-display");
    $("#load_diagram_pop").addClass("block");
    $("#load_diagram_pop").addClass("overlay");
    
    location.href = "#load_diagram_pop";

    $(".btn-cancel").click(function () {
        $("#load_diagram_pop").removeClass("block");
        $("#load_diagram_pop").removeClass("overlay");
        $("#load_diagram_pop").addClass("no-display");

        location.href = "#";
    });

    $('#btn_load_ok').off('click');
    $('#btn_load_ok').on('click');
    $('#btn_load_ok').click(function () {
        func_load_func();
        res = [];
    });

    var fileInput = document.getElementById("csv"),
    readFile = function () {
        var reader = new FileReader();
        reader.onload = function () {
            res = csvJSON(reader.result);
            $('#btn_load_ok').removeAttr('disabled');
        };
        reader.readAsText(fileInput.files[0], 'utf8');
        
    };
    fileInput.addEventListener('change', readFile);
}

function csvJSON(csv) {

    var lines = csv.split("\n");

    var result = [];

    var headers = lines[0].split(",");
    

    for (var i = 1; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(",");

        for (var j = 0; j < headers.length; j++) {
            headers[j] = headers[j].trim();
            obj[headers[j]] = currentline[j];
        }

        result.push(obj);

    }
    return result;
}

function func_load_func() {
    var flag = 0;
    var keys = Object.keys(res[0]);
    current_selected_provider_id = res[0]["Provider"];
    current_selected_table_id = "";

    current_selected_row = "";
    current_sel_row_num;

    provider_list = [];
    provider_list.push(res[0]["Provider"]);

    table_id_list = {};
    //table_id_list = { res[0]["Provider"]: [] };
    table_id_list[res[0]["Provider"]] = new Array();

    table_column_list = {};
    //table_column_list = { "Provider1": {} };
    table_column_list[res[0]["Provider"]] = new Object();

    var old_provider = res[0]["Provider"];
    var old_table = (res[0]["TargetTable"].split("."))[1];

    table_column_list[old_provider][old_table] = new Array();
    table_id_list[old_provider].push(old_table);
    schema_sourcefilename_list = {};
    schema_sourcefilename_list[old_provider] = new Object();
    schema_sourcefilename_list[old_provider][old_table] = new Object({"schema": (res[0]["TargetTable"].split("."))[0], "sourcefilename": res[0]["SourceFile"]});

    res.forEach(function (element) {
        if (element["Provider"] != "") {
            var foreignKey = "";
            if (element["PKColumnRank"] != "") {
                foreignKey = "Yes";
            }
            else {
                if (element["ForeignKey"] == "") {
                    foreignKey = "No";
                }
                else {
                    foreignKey = element["ForeignKey"];
                }
            }

            if (old_provider == element["Provider"] && old_table == (element["TargetTable"].split("."))[1]) {
                var arr = new Object({ "Column Name": element["TargetColumn"], "DataType": element["DataType"], "Length": element["Length"], "Scale": element["Scale"], "Nullable": element["IsNullable"], "Default Value": element["Default"], "IsIdentity": element["IsIdentity"], "IsVersioning": element["IsVersioning"], "IsDateStamp": element["IsDateStamp"], "IsCalculated": element["IsCalculated"], "ValidateFor": element["ValidateFor"], "EnableDate": element["EnableDate"], "DisableDate": element["DisableDate"], "RenameDate": element["RenameDate"], "RenameColumnAs": element["RenameColumnAs"], "RenameDWColumnAs": element["RenameDWColumnAs"], "IsIndexed": element["IsIndexed"], "Remarks": element["Remarks"], "IsPrimary": foreignKey, "SurrogateFKName": element["SurrogateFKName"], "PKColumnRank": element["PKColumnRank"], "SourceColumn": element["SourceColumn"] });
                table_column_list[old_provider][old_table].push(arr);
            }

            else if (old_provider == element["Provider"] && old_table != (element["TargetTable"].split("."))[1]) {
                old_table = (element["TargetTable"].split("."))[1];

                table_id_list[old_provider].push(old_table);

                schema_sourcefilename_list[old_provider][old_table] = new Object({ "schema": (res[0]["TargetTable"].split("."))[0], "sourcefilename": res[0]["SourceFile"] });

                table_column_list[old_provider][old_table] = new Array();
                var arr = new Object({ "Column Name": element["TargetColumn"], "DataType": element["DataType"], "Length": element["Length"], "Scale": element["Scale"], "Nullable": element["IsNullable"], "Default Value": element["Default"], "IsIdentity": element["IsIdentity"], "IsVersioning": element["IsVersioning"], "IsDateStamp": element["IsDateStamp"], "IsCalculated": element["IsCalculated"], "ValidateFor": element["ValidateFor"], "EnableDate": element["EnableDate"], "DisableDate": element["DisableDate"], "RenameDate": element["RenameDate"], "RenameColumnAs": element["RenameColumnAs"], "RenameDWColumnAs": element["RenameDWColumnAs"], "IsIndexed": element["IsIndexed"], "Remarks": element["Remarks"], "IsPrimary": foreignKey, "SurrogateFKName": element["SurrogateFKName"], "PKColumnRank": element["PKColumnRank"], "SourceColumn": element["SourceColumn"] });
                table_column_list[old_provider][old_table].push(arr);
            }

            else if (old_provider != element["Provider"] && element["Provider"] != "") {
                old_provider = element["Provider"];
                old_table = (element["TargetTable"].split("."))[1];

                provider_list.push(old_provider);
                table_id_list[old_provider] = new Array();
                table_id_list[old_provider].push(old_table);
                table_column_list[old_provider] = new Object();
                table_column_list[old_provider][old_table] = new Array();
                var arr = new Object({ "Column Name": element["TargetColumn"], "DataType": element["DataType"], "Length": element["Length"], "Scale": element["Scale"], "Nullable": element["IsNullable"], "Default Value": element["Default"], "IsIdentity": element["IsIdentity"], "IsVersioning": element["IsVersioning"], "IsDateStamp": element["IsDateStamp"], "IsCalculated": element["IsCalculated"], "ValidateFor": element["ValidateFor"], "EnableDate": element["EnableDate"], "DisableDate": element["DisableDate"], "RenameDate": element["RenameDate"], "RenameColumnAs": element["RenameColumnAs"], "RenameDWColumnAs": element["RenameDWColumnAs"], "IsIndexed": element["IsIndexed"], "Remarks": element["Remarks"], "IsPrimary": foreignKey, "SurrogateFKName": element["SurrogateFKName"], "PKColumnRank": element["PKColumnRank"], "SourceColumn": element["SourceColumn"] });
                table_column_list[old_provider][old_table].push(arr);

                schema_sourcefilename_list[old_provider] = new Object();
                schema_sourcefilename_list[old_provider][old_table] = new Object({ "schema": (res[0]["TargetTable"].split("."))[0], "sourcefilename": res[0]["SourceFile"] });
            }
        }
    });

    location.href = "#";

    var str = "";
    for (var i = 0; i < provider_list.length; i++) {
        str += "<option>" + provider_list[i] + "</option>";
    }    

    $("#provider_select").html(str);

    Display_table_name_list();
    redraw_all_table();
    redraw_all_connection();
    
}

function Edit_Provider() {
    previous_provider = $("#provider_select").val();
    $("#edit_provider_pop").removeClass("no-display");
    $("#edit_provider_pop").addClass("block");
    $("#edit_provider_pop").addClass("overlay");

    location.href = "#edit_provider_pop";
}

function Del_Provider() {
    var del_provider_id = $("#provider_select").val();

    if (provider_list.length == 1) {
        alert("You can't delete this Provider!!!");
        return;
    }

    else {
        $("#provider_select option").each(function () {
            if ($(this).html() == del_provider_id) {
                $(this).remove();
            }
        });

        provider_list.pop(del_provider_id);
        delete table_id_list[del_provider_id];
        delete table_column_list[del_provider_id];

        if (current_selected_provider_id == del_provider_id) {
            $("#provider_select").val($("#provider_select option:first").val());
            current_selected_provider_id = $("#provider_select").val();

            Display_table_name_list();
            redraw_all_table();
            redraw_all_connection();
        }
    }
}

function func_editTbl(param){
    current_selected_table_id = param;
    $("#edit_table_pop").find('input').removeClass('alert-input');
    $("#edit_pop_tablename_input").val(current_selected_table_id);
    $("#edit_input-schema").val(schema_sourcefilename_list[current_selected_provider_id][current_selected_table_id]["schema"]);
    $("#edit_input-sourcefilename").val(schema_sourcefilename_list[current_selected_provider_id][current_selected_table_id]["sourcefilename"]);
    $("#edit_table_pop").removeClass("no-display");
    $("#edit_table_pop").addClass("block");
    $("#edit_table_pop").addClass("overlay");

    location.href = "#edit_table_pop";
}